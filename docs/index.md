# Cove Deployment Modules

Extendable CI job templates for building and deploying your apps to Cove quickly
and uniformly.

See the [Cove deployment documentation] for how this is used.
If you're new here, that will provide a better starting point for understanding
what this project is and why it exists.

## Getting started

### Setup

If you're starting from scratch, fork the [example project] then follow its
quickstart.

Otherwise, if you've got existing CI that you want to incorporate the Cove
Deployment Modules into, add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - project: cloud-native/cove-deployment-modules
    ref: v8
    file: cove-agent-deploy.yaml
```

If you want to use a specific version, be sure to update the `ref` field
accordingly and also set the `COVE_DEPLOYMENT_MODULES_VERSION` variable.

```yaml
include:
  - project: cloud-native/cove-deployment-modules
    ref: v8.7.0
    file: cove-agent-deploy.yaml

variables:
   COVE_DEPLOYMENT_MODULES_VERSION: v8.7.0
```

Also, depending on the [job templates](#extendable-jobs) you use, you'll have to
include the relevant stages used by these jobs:

```yaml
stages:
  - build
  - prepare
  - validate
  - deploy
  - verify
  - cleanup
```

You can, of course, override these stages if required.

#### Using an existing namespace with CDM

If you're using CDM to deploy to an existing namespace you'll need to change
some annotations on it to satisfy CDM's safety checks. You'll need to set
`catalystcove.nz/gitlab-agent` to match your `COVE_AGENT` and
`catalystcove.nz/managed-by` to match `COVE_TEAM` (if you haven't set this, it
defaults to your GitLab project path).

### Create a GitLab Deploy Token

This is the credential your Kubernetes cluster uses to push/pull Docker images
and helm packages to/from your GitLab project. GitLab can generate these on the
fly for use during a job, but they expire too quickly. So, we need to create
this token so that GitLab will use it instead.

1. In the GitLab Web UI Navigate to `Settings -> Repository -> Deploy Tokens`
1. Create a token named `gitlab-deploy-token` with access to everything.
   You don't have to do anything with this token - it's used by GitLab
   automatically for accessing resources in your project.
   See the [GitLab deploy token] docs for more details.

### Extend!

You are now ready to start using the [extendable job templates](#extendable-jobs)
below in your CI.

## More Examples

Also see [these example projects][other examples] that might fit your scenario
better.

## Extendable Jobs

Below is an exhaustive list of all the job templates that this library provides.
Use the [`extends:`][extends] keyword in your job to include these templates:

!!! note
    Any cluster-specific job should set the [`COVE_AGENT`](#the-cove_agent-variable)
    variable.

### `.lint-dockerfile`

Extend this to lint your `Dockerfile` and ensure you're following best practice.

Example:

```yaml
lint-dockerfile:
  extends: .lint-dockerfile
```

If you are building docker images outside of the Cove Deployment Modules
framework you can still easily include this linter by adding to your
`.gitlab-ci.yml` the following

```yaml
include:
  - project: cloud-native/cove-deployment-modules
    ref: v8
    file: cove-dockerfile-lint.yaml
```

This job uses [Hadolint] to check your Dockerfile for issues, you
should read the documentation on its github page to find out how to address
issues it may alert you to.

But briefly, to ignore failures, you simply need to add inline ignores in your
Dockerfile [like this][hadolint ignores] referencing the particular rule that
your Dockerfile has broken -- e.g. **DL3009** if you've not deleted the apt
lists after an `apt-get update`.

Note that Hadolint has been configured to ignore **DL3005** by default.
You can customize these rule exclusions, as well as other Hadolint options
by setting the `HADOLINT_OVERRIDES` variable to something else, e.g.
`--ignore DL3005 --ignore DL3008`. See the [Hadolint rules docs][hadolint rules]
for further details.

Failures at `info` level and higher will fail the job. This is fairly strict, so
if you want to change the failure threshold, set the `HADOLINT_FAILURE_THRESHOLD`
environment variable to something else, e.g. `warning` or `error`. See the
[Hadolint configuration docs][hadolint config] for more information.

Note this is by default in the **build** stage, but you can override that when
you extend the template, as you may have a testing stage where having this job
run makes more sense.

If your job is running as part of a Merge Request, you will be able to see the
a code quality report in the main merge request screen, however this will only
work if the code quality report has already run against the branch you want
to merge into because of [this limitation/bug][codequality gitlab limitation]
in Gitlab

### `.build-image`

The Cove Deployment Modules expects your project to have a Dockerfile, from
which it builds container images for your app.

It builds images from your project's `Dockerfile` and pushes them to your
project's container registry in GitLab. It also produces image digest files,
as artifacts and adds the GitLab pipeline, job number and Git commit id as
metadata in the image.

Example:

```yaml
build-example:
  extends: .build-image
  variables:
    BUILD_NAME: example
```

To support multiple Docker images per Dockerfile, you do need to name the build
stages (targets) in your Dockerfile, even if there is only one.
For example:

```dockerfile
FROM harbor.catalyst.net.nz/catalyst/ubuntu:jammy AS example
```

`BUILD_NAME` is used to specify a [build stage] in your `Dockerfile` and name
the resulting image. This variable is currently required, even if your
`Dockerfile` only includes one build stage.

If you need to build more than one image, define more jobs that `extends:
.build-image` and adjust the `BUILD_NAME` variable.

If you want to name your images independently from their build stages, you can
override the `BUILD_TARGET` variable, at which point:

- `BUILD_TARGET` will specify the [build stage] in your `Dockerfile`
- `BUILD_NAME` will be used to name the resulting image

By default, the image build runs from the root of your project.
However, if you want to use a different directory or filename for your
`Dockerfile`, configure the `BUILD_CONTEXT` and/or `DOCKERFILE_LOCATION`
[variables](#variables).

[kaniko] is used for building the images.
You can specify additional command flags by setting the `KANIKO_FLAGS` and
`KANIKO_FLAGS_CACHE` [variables](#variables).

If your build job runs the same commands every time, but you don't ADD or COPY
different files, you will probably run into cache problems during image
creation time, as the build process will take previous build layers from cache.
To defeat this behavior, in your Dockerfile, add the following line before the
problematic layer to invalidate the cache for subsequent layers.

```dockerfile
ARG CACHEBUST=1
```

### `.copy-image`

Optional.

Allows you to push your built images to another location.
This can be used to cross-publish images (for example to [Harbor]), or to
"promote" an image from one tag to another.

For example, the following job definition will push the image produced by
[`.build-image`](#build-image) to the specified project on
`harbor.catalyst.net.nz` under the same tag:

```yaml
publish-to-harbor:
  extends: .copy-image
  variables:
    BUILD_NAME: example
    IMAGE_REPOSITORY: harbor.catalyst.net.nz/<project>/<repository>
    IMAGE_REGISTRY_USER: <harbor-username>
    IMAGE_REGISTRY_PASSWORD: <harbor-password> # set this via masked CI variable
```

If you do not specify an `IMAGE_REPOSITORY`, the job will publish back to
GitLab. This makes it easy to re-tag the images produced by `.build-image`, for
example to identify a specific build of the app.

```yaml
tag-image:
  extends: .copy-image
  variables:
    BUILD_NAME: example
    IMAGE_TAG: <image-tag> # for example "uat"
```

!!! note
    Currently, you must specify a `BUILD_NAME` value for both `.build-image`
    and `.copy-image`, in order to link the two operations.

[Harbor]: https://wiki.wgtn.cat-it.co.nz/wiki/Harbor

### `.scan-image`

Optional.

This job scans a container image for vulnerabilities, producing a security
report and optionally failing the build if critical vulnerabilities are
detected. By default, it will scan the image produced in a previous
`.build-image` job.

```yaml
scan-image:
  extends: .scan-image
  allow_failure: true
  variables:
    BUILD_NAME: example
```

!!! note
    Currently, you must specify a `BUILD_NAME` value for both `.build-image`
    and `.scan-image`, in order to link the two operations.

As configured above, this job will always succeed and simply issue a warning
when vulnerabilities are found. You can cause critical vulnerabilities to fail
the pipeline by changing `allow_failure: true` to `false`.

To scan an arbitrary image, supply `IMAGE_REPOSITORY` and/or `IMAGE_TAG`, as
well as `IMAGE_REGISTRY_USER` and `PASSWORD` if accessing the image requires
authentication (for example in a private project on Harbor):

```yaml
scan-image:
  extends: .scan-image
  variables:
    IMAGE_REPOSITORY: harbor.catalyst.net.nz/<project>/<repository>
    IMAGE_TAG: <tag> # defaults to the tag produced by the current pipeline
    IMAGE_REGISTRY_USER: <harbor-username>
    IMAGE_REGISTRY_PASSWORD: <harbor-password> # set this via masked CI variable
```

This job will generate a [CycloneDX-formatted SBOM] as an artifact, which is
available to subsequent jobs in the file `cyclonedx-report.json`.

[CycloneDX-formatted SBOM]:
 https://aquasecurity.github.io/trivy/v0.41/docs/supply-chain/sbom/#cyclonedx

### `.lint-helm-chart`

Optional.

```yaml
lint-helm-chart:
  extends: .lint-helm-chart
```

If you're building Helm charts outside of the Cove Deployment Modules
framework you can still easily include this linter by adding to your
`.gitlab-ci.yml` the following:

```yaml
include:
  - project: cloud-native/cove-deployment-modules
    ref: v8
    file: cove-helm-build.yaml
```

This job uses:

1. [`helm lint`](https://helm.sh/docs/helm/helm_lint/) to ensure the chart is well formed
1. [`helm template`](https://helm.sh/docs/helm/helm_template/) to render the chart templates
1. [`kube-score`](https://github.com/zegl/kube-score) to validate the rendered chart templates

Have a look at the [kube-score checks] to find out how to address issues it may
alert you to.

The following `variables` can be set on your job, in order to control the
behaviour of the Helm template generation and kube-score:

- `CHART_DIR`: the directory your chart lives in.
  This defaults to `$COVE_CHART_DIR`, so you'll only need to override/set this
  if you're not using the Cove Deployment Modules for your deployments.
- `HELM_FLAGS`: use this to specify any additional `helm` command flags to set.
- `KUBE_SCORE_FLAGS`: override this to specify your own `kube-score` flags.
  Some sane flags are set by default.

Note this is by default in the **build** stage, but you can override that when
you extend the template, as you may have a testing stage where having this job
run makes more sense.

!!! note
    The linter may recommend you add ephemeral storage limits to your chart.
    Even if you don't save anything to disk, anything your application logs
    to stdout or stderr will count against this quota.
    K8s keeps up to 50mb of logs, so you should never set your quota at or
    below this limit.

### `.build-helm-chart`

Optional.

If your project includes its own custom Helm chart (`COVE_CHART_DIR`), this job
can package and publish it to a Helm repository (`COVE_CHART_REPO`).

This job is not a requirement for deployment of your in-project chart - see
[`the deploy job`](#deploy-cove-app) for details on that.

Configure `COVE_CHART_DIR` to point to the chart directory in your project.
Here's an example:

```yaml
build-helm:
  extends: .build-helm-chart
  variables:
    COVE_CHART_DIR: helm/example-chart
```

If you're building Helm charts outside of the Cove Deployment Modules framework
you can still use this job by adding the following to your`.gitlab-ci.yml`:

```yaml
include:
  - project: cloud-native/cove-deployment-modules
    ref: v8
    file: cove-helm-build.yaml
```

By default, this job will publish the chart to your GitLab project's Helm repo.
To publish your helm chart to a different Helm repo, configure `COVE_CHART_REPO`,
`COVE_CHART_USER` and `COVE_CHART_PASSWORD`.
See the [Variables](#variables) section for more details.

```yaml
build-helm:
  extends: .build-helm-chart
  variables:
    COVE_CHART_DIR: helm/example-chart
    COVE_CHART_REPO: https://harbor.catalyst.net.nz/chartrepo/example-repo
    COVE_CHART_USER: example-username
    COVE_CHART_PASSWORD: example-password
```

!!! note
    Use this if your chart is only useful for your project, and not to other
    projects/teams.
    If your chart is re-usable, consider getting it added to [Cloud Services'
    Helm charts][cloud services helm charts], which builds and pushes the chart
    to Catalyst's Helm repo.
    Ask the Cloud Services team to assist with this.

### `.build-values-env`

Optional.

Generates a `values.env` file artifact containing variables and values you
want to use to replace corresponding placeholders in your `values.yaml`
(in the next job).

You can customise the path used for this file by setting `VALUES_ENV`.
The file will be created if it does not exist and is empty by default.

You should extend this job to have your own entries added.
Your entries can either come from:

- A file pointed to by `VALUES_ENV` (default: `values.env`) in your codebase, and/or
- Entries appended to the file as part of the job

An example might look like:

```yaml
build-values-env:
  extends: .build-values-env
  script:
    - echo "ENVIRONMENT=development" >> ${VALUES_ENV}
    - echo "SOME_VAR=somevalue" >> ${VALUES_ENV}
```

### `.build-values-yaml`

Firstly, the job appends to `values.env`, any image digests produced by
[image build jobs](#build-image). These are in the form of
`${BUILD_NAME}_DIGEST=sha256:[somesum]`, where `$BUILD_NAME` has been uppercased
and any non-alphanumeric characters replaced with underscores. For example, a
`$BUILD_NAME` value of `nginx-frontend` will result in
`NGINX_FRONTEND_DIGEST=sha256:[somesum]` to be appended to `values.env`. This
enables you to reference image digests via these placeholders in your
`values.yaml` file, instead of using tags, if you wanted.

Next, it takes your Helm values file (configured via the `VALUES_FILE` variable)
and replaces the placeholders therein with corresponding values from
`values.env` and the environment, in order to generate the final values file to
be used for deployment.
The values file will be created if it does not exist.

Note the following snippet from a `values.yaml`.

```yaml
image:
  repository: ${IMAGE_REPOSITORY}
  # If you're using digests you could set this to IfNotPresent.
  pullPolicy: Always
  # tagPhp is the tag to use to pull PHP-FPM image for the project.
  tagPhp: php-${CI_COMMIT_SHORT_SHA}
  # digestNginx is the digest to use to pull the NGINX image for the project.
  digestNginx: ${NGINX_FRONTEND_DIGEST}
```

On deployment, Cove will provision a docker registry secret in your namespace to
ensure access to your built images.
The name for this secret comes from configuration, so be sure to add the
following to your  `values.yaml`, so your images can be pulled:

```yaml
imagePullSecrets:
  - name: ${IMAGE_REGISTRY_SECRET_NAME}
```

Be sure to set the correct `IMAGE_REPOSITORY` variable here, if you're
publishing your images to an external image registry.

Required variables:

- `COVE_AGENT`
- `COVE_NAMESPACE`

### `.validate-cove-app`

Optional. Depends on [`.build-values-yaml`](#build-values-yaml).

This job validates your deployment by dry-running an apply of any pre-deploy
manifests (`COVE_PRE_DEPLOY_MANIFEST_DIR`), together with the generated Helm
chart templates, against your cluster.

Set the [`COVE_AGENT`](#the-cove_agent-variable) variable to target a specific
cluster for validation. You add a variable to your `.gitlab-ci.yml` file like so:

```yaml
validate-staging:
  extends: .validate-cove-app
  variables:
    COVE_AGENT: staging
```

Note this now means you might need two or more jobs that do same thing, but have
different rules to trigger them, so you will probably end up with jobs that look
like this:

```yaml
validate-staging:
  extends: .validate-cove-app
  variables:
    COVE_AGENT: staging
  rules:
    - if: '$CI_COMMIT_REF_NAME == "staging"'

validate-production:
  extends: .validate-cove-app
  variables:
    COVE_AGENT: production
  rules:
    - if: '$CI_COMMIT_REF_NAME == "production"'
```

When `COVE_HELM_DIFF` is set, this job will output a `diff` comparing your
planned changes to what's already in the cluster. When releasing something for
the first time, this will of course include everything. After that, changes
will be highlighted with green lines being added, red lines being removed.

!!! caution
    Like all CI output, the generated diff may include secrets, either directly
    or in base64-encoded form. If this is a problem, either leave diffing
    disabled or add [masked CI variables] for both forms of the value to
    prevent them from displaying in the output.

!!! note
    The validation here excludes Helm hooks and `RoleBinding` definitions.

### `.deploy-cove-app`

Depends on [`.build-values-yaml`](#build-values-yaml).

This is finally where the magic happens.

The deploy job installs or updates your configured Helm chart on your cluster.
As with the validate job, you can set [`COVE_AGENT`](#the-cove_agent-variable)
to choose a target cluster for the deploy.
The same goes for the `COVE_RANCHER_PROJECT_ID` variable - set this to the correct
project on the correct cluster.

Every time a deploy job runs it will create or update an application in
Rancher using the configured Helm chart.
The `COVE_CHART_` variables you set determine where your chart will be fetched
from for deployment:

- If you've only set `COVE_CHART_DIR` (indicating an in-project chart), this job
  will deploy the chart straight from the configured local chart directory.

- If you've set `COVE_CHART_NAME` and `COVE_CHART_VERSION`, this job will fetch
  the Helm chart from the configured remote Helm repo (`COVE_CHART_REPO`).

The name of the application will be what you have configured
[`COVE_NAMESPACE`](#the-cove_namespace-variable) to.

This job also sets up the docker registry secret, used to pull your images.
So, be sure to set the correct `IMAGE_REPOSITORY`, `IMAGE_REGISTRY_USER` and
`IMAGE_REGISTRY_PASSWORD`variables here, if your app will be pulling images
from an external image registry.

### `.delete-cove-app`

Optional.

Delete your deployment.
It is also used by the environments interface to stop your GitLab environments.
As with the validate and deploy jobs, you can set [`COVE_AGENT`](#the-cove_agent-variable)
to choose a target cluster for this job.

### `.cove-admin`

Optional.

You can extend this to get access to some additional tooling that could be used in
your pipeline jobs.

The [cove-tools] image is used here, so you get access to all its tools, with
`kubectl` and `helm` pre-configured to just work.

Simply extending `.cove-admin` in your job does nothing - you should also have
a `script` block where you utilise the pre-configured tools as needed.

Below is an example of a post-deploy verification job using `.cove-admin`.
`kubectl` is used to block until an application's pods are running, wait for
version updates to complete, print logs from a post-install job, and finally to
dump five minutes' worth of logs from the deployment:

```yaml
verify-staging:
  extends: .cove-admin
  stage: verify
  variables:
    COVE_AGENT: staging
  script:
    - kubectl wait --for condition=available deploy/example-application
    - kubectl rollout status --timeout 5m deploy/example-application
    - kubectl logs job/example-application-post-install
    - kubectl logs --since 5m deploy/example-application
```

## Variables

The following variables can be set, depending on your workflow, at the project,
pipeline, or job level to configure the behaviour of the deployment modules.

| Name  | Usage | Default |
| ----- | ----- | -----   |
| `BUILD_NAME` | The name for your container image (used by [`.build-image`](#build-image) and [`.copy-image`](#copy-image)). | `''` |
| `BUILD_TARGET` | The [build stage] in your Dockerfile to build (used by [`.build-image`](#build-image)). | `${BUILD_NAME}` |
| `BUILD_CONTEXT` | The [build context] for your image (used by [`.build-image`](#build-image)). | `$CI_PROJECT_DIR` |
| `COVE_AGENT_REPOSITORY` | The repository path where the cove agent is connected and configured. | `$CI_PROJECT_PATH` |
| `COVE_AGENT` | See [details here](#the-cove_agent-variable). Used by [`.build-values-yaml`](#build-values-yaml) | `''` |
| `COVE_CHART_DIR` | The path to your in-project Helm chart. | `''` |
| `COVE_CHART_NAME` | The Helm chart name to build and/or deploy. Used by [`.build-helm-chart`](#build-helm-chart) and [`.deploy-cove-app`](#deploy-cove-app), so setting this variable globally is usually what you want. | `''` |
| `COVE_CHART_PASSWORD` | The password for Helm repo authentication. Used by [`.build-helm-chart`](#build-helm-chart) and [`.deploy-cove-app`](#deploy-cove-app), so setting this variable globally is usually what you want. | `$CI_DEPLOY_PASSWORD` |
| `COVE_CHART_REPO` | The Helm repo URL to use for publishing and/or deploying the specified Helm chart. Used by [`.build-helm-chart`](#build-helm-chart) and [`.deploy-cove-app`](#deploy-cove-app), so setting this variable globally is usually what you want. | `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable` |
| `COVE_CHART_USER` | The user for Helm repo authentication. Used by [`.build-helm-chart`](#build-helm-chart) and [`.deploy-cove-app`](#deploy-cove-app), so setting this variable globally is usually what you want. | `$CI_DEPLOY_USER` |
| `COVE_CHART_VERSION` | The Helm chart version to deploy. Used by [`.build-helm-chart`](#build-helm-chart) and [`.deploy-cove-app`](#deploy-cove-app), so setting this variable globally is usually what you want. | `''` |
| `COVE_HELM_DEPLOY_FLAGS` | Additional `helm` flags to set when running this command at deploy time. For example `--debug`. | `'--wait --wait-for-jobs --timeout=30m'` |
| `COVE_HELM_DIFF` | Whether to display the changes that will be applied (used by [`.validate-cove-app`](#validate-cove-app)). | `'false'` |
| `COVE_HELM_DIFF_COMMAND` | A command that is used to display (or not, if "false") the changes that will be applied (used by [`.validate-cove-app`](#validate-cove-app)). | `'colordiff -N -u'` |
| `COVE_NAMESPACE` | See [details here](#the-cove_namespace-variable). | `$CI_PROJECT_PATH_SLUG-$CI_COMMIT_REF_SLUG` |
| `COVE_POD_SECURITY_ENFORCE` | Level for `pod-security.kubernetes.io/enforce` - see https://kubernetes.io/docs/concepts/security/pod-security-admission | `privileged` |
| `COVE_POD_SECURITY_WARN` | Level for `pod-security.kubernetes.io/warn` - see https://kubernetes.io/docs/concepts/security/pod-security-admission | `restricted` |
| `COVE_PRE_DEPLOY_MANIFEST_DIR` | A repository path that points to a directory with k8s manifests to install before the main app. For example, sealed secrets should go here. | `''` |
| `COVE_RANCHER_PROJECT_ID` | See [details here](#the-cove_rancher_project_id-variable). | `''` |
| `COVE_TEAM` | The team looking after this project. Used for app metadata. | `$CI_PROJECT_PATH` |
| `DOCKERFILE_LOCATION` | The path to your `Dockerfile` (used by [`.build-image`](#build-image)). | `Dockerfile` |
| `IMAGE_DIGEST_EXTENSION` | The extension of the file that contains the digest of the built image. The filename is always the `BUILD_NAME` (used by [`.build-image`](#build-image)). | `image.digest.cove` |
| `IMAGE_REGISTRY_PASSWORD` | Password for the destination `IMAGE_REPOSITORY` (used by [`.copy-image`](#copy-image) and [`.deploy-cove-app`](#deploy-cove-app)) | `$CI_DEPLOY_PASSWORD` |
| `IMAGE_REGISTRY_SECRET_NAME` | The name of Docker registry secret used when pulling your built images. You will probably never have to change this. | `docker-registry` |
| `IMAGE_REGISTRY_USER` | Username for the destination `IMAGE_REPOSITORY` (used by [`.copy-image`](#copy-image) and [`.deploy-cove-app`](#deploy-cove-app)) | `$CI_DEPLOY_USER` |
| `IMAGE_REPOSITORY` | The destination repository for your published image (used by [`.copy-image`](#copy-image) and [`.deploy-cove-app`](#deploy-cove-app)) | `$CI_REGISTRY_IMAGE` |
| `IMAGE_TAG` | The destination tag for your published image (used by [`.copy-image`](#copy-image)) | `$BUILD_NAME-$CI_COMMIT_SHORT_SHA` |
| `KANIKO_FLAGS` | Custom flags that will be passed to [Kaniko] (used by [`.build-image`](#build-image)). | `''` |
| `KANIKO_FLAGS_CACHE` | Custom flags related to caching, that will be passed to [Kaniko] (used by [`.build-image`](#build-image)). | `--cache-repo docker-registry.cove-docker-registry/cache --skip-tls-verify-registry docker-registry.cove-docker-registry` |
| `VALUES_ENV` | The path to your `values.env` file (used by [`.build-values-env`](#build-values-env)). | `values.env` |
| `VALUES_FILE`| The path to your Helm `values.yaml` file, or files as a space seperate list merged in order (used by [`.build-values-yaml`](#build-values-yaml)). | `values.yaml` |
| `VALUES_SUBSTITUTE_VARS` | Used by [`.build-values-yaml`](#build-values-yaml), only substitute these variables in the values file. Use a comma-separated list, with double dollars in front of variable names e.g `$$VAR1,$$VAR2`. By default, all variables are considered for substitution | `''` |

Apart from `COVE_AGENT`, all of these variables are optional.
If specified, all paths must be relative to your project root.

!!! caution
    Consider the [GitLab CI/CD variable precedence][gitlab-ci-var-precedence]
    when overriding config. The rules are all fairly simple, but there are
    enough of them that it's easy to get lost if you're not careful.

### The `COVE_NAMESPACE` variable

This is the most important variable to understand, since it's used as an input
to generate names for several things in your deployment.

Specifically, it is used as:

1. The namespace where your application will be installed,
1. The name of the [Helm release] for your application (effectively an instance ID, unique within the cluster), and
1. The prefix for all resources created by your Helm chart (conventionally, at least!)

It is often used as part of the DNS name for your application, too,
particularly if you started from the [example project] (see the
[DNS management section] of the Cove docs for details).

You do have some control over item three -- your Helm chart can create whatever
names you'd like, although third-party charts will of course apply their own
names -- but we don't recommend departing too far from existing conventions.

It's also important that all of these things meet the [K8s naming constraints
for DNS label names], so please see the caution about length limits in the next
section!

Changing this value will cause a new application to be created.

### The `COVE_AGENT` variable

The name of a [GitLab Kubernetes agent] connected to you project, to use for CI.

Set this variable globally if you have a single cluster, or at a job level if
you're deploying to multiple clusters.

Use this variable, together with `COVE_AGENT_REPOSITORY` when you're using a
shared/group agent.

### The `COVE_RANCHER_PROJECT_ID` variable

This variable dictates into which Rancher project your app will be deployed.
Get this value from `metadata.name` when viewing the the project's yaml in the
[Rancher UI].
NOTE that this value is *not* the name of your project - it should be in the
form of `p-xxxxx`!

If you're deploying to multiple clusters, or you want to use different projects
on the same cluster, ensure you configure this for each
[`.deploy-cove-app`](#deploy-cove-app) job.

## GitLab Environments

You can use GitLab's [Environments] feature to provide a view of what you have
deployed. Navigate to `Operations -> Environments` to see all the configured
apps for your project. You can stop the environments here which is helpful for
transient apps.

!!! caution
    It's easy to hit name length limits in Kubernetes, so keep your project and
    app/branch names short. Subcharts (especially Postgres) make very
    long deployment names in the cluster and you could end up exceeding the
    limits. So use abbreviations when naming your projects and branches, eg
    "MOF" not "ministry-of-food" and "prod" not "production". This comes from
    the Kubernetes limit of 63 characters for a label, which are used by Helm
    and Rancher to keep track of what is deployed. A concatenation of
    `$projectname-$branchname-$subchartname-$someotherthing` can easily exceed
    this.

## Troubleshooting

### Deploy validation failed 👎️

The GitLab CI logs just above this message should indicate the cause. The
validation step is covered in more detail [above](#validate-cove-app).

### IMAGE_REGISTRY_USER is not set correctly 👎

Check your [deploy token](#create-a-gitlab-deploy-token), as it might have expired or changed.

### XYZ is not set 👎

This is a variable that must be set by the user, but it wasn't assigned a value
in this pipeline. Check the list of [variables](#variables) above for
information on the specific variable and how it should be set.

### Unable to set up temporary namespace for validation

Usually your `COVE_NAMESPACE` is used for validation. However, this may not
exist if you have not yet run a deploy. In this case, a temporary namespace
will be used instead. This error occurs when the temporary namespace cannot be
created. The GitLab CI logs just above this message should indicate the cause.
Usually, this is caused by another variable with an incorrect setting, but it
may also be a permissions issue if your CI job is not allowed to create
arbitrary namespaces.

[build stage]: https://docs.docker.com/develop/develop-images/multistage-build/#name-your-build-stages
[build context]: https://docs.docker.com/build/concepts/context/
[cloud services helm charts]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/helm-charts
[codequality gitlab limitation]: https://gitlab.com/gitlab-org/gitlab/-/issues/215279
[cove deployment documentation]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/
[cove-tools]: https://harbor.catalyst.net.nz/harbor/projects/3/repositories/cove-tools
[environments]: https://docs.gitlab.com/ee/ci/environments/
[DNS management section]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/dns/
[example project]: https://gitlab.catalyst.net.nz/cove/example-project
[extends]: https://docs.gitlab.com/ee/ci/yaml/#extends
[gitlab-ci-var-precedence]: https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
[gitlab deploy token]: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#gitlab-deploy-token
[gitlab kubernetes agent]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#gitlab-kubernetes-agents
[hadolint]: https://github.com/hadolint/hadolint
[hadolint ignores]: https://github.com/hadolint/hadolint#inline-ignores
[hadolint config]: https://github.com/hadolint/hadolint#configure
[hadolint rules]: https://github.com/hadolint/hadolint#rules
[helm release]: https://helm.sh/docs/glossary/#release
[kaniko]: https://github.com/GoogleContainerTools/kaniko
[masked CI variables]: https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable
[other examples]: https://gitlab.catalyst.net.nz/cove/example-project#other-examples
[k8s naming constraints for dns label names]: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-label-names
[kube-score checks]: https://github.com/zegl/kube-score#checks
[rancher ui]: https://rancher.catalystcove.nz/
