## Contribution Workflow

Development should always happen in feature branches, which are then merged
into `master`. Tags `v7.0.0`, `v7.1.0`, etc. are used to track [semantically
versioned releases], and branches `v7`, `v8`, etc. are maintained for the
latest release of each major version.

![Git graph](./git-graph.png)

<!--

Above image was generated using the following Mermaid diagram, which doesn't
render in GitLab. If this is ever fixed, we should be able to uncomment this
fenced block and drop the image:

```mermaid
%%{init: { 'theme': 'default', 'gitGraph': { 'mainBranchName': 'master', 'showCommitLabel': false }}}%%
gitGraph
  commit
  branch v7 order: 1
  branch feature-one order: 4
  checkout feature-one
  commit
  commit
  checkout master
  merge feature-one
  checkout v7
  merge master tag: "v7.0.0"
  checkout master
  branch feature-two order: 5
  commit
  checkout v7
  branch bugfix-v7 order: 3
  commit
  checkout v7
  merge bugfix-v7 tag: "v7.0.1"
  checkout master
  merge v7
  checkout feature-two
  commit
  checkout master
  merge feature-two
  checkout v7
  merge master tag: "v7.1.0"
  checkout master
  branch v8 order: 2
  commit tag: "v8.0.0"
```
-->

When contributing here please also consider what documentation/examples need
updating:

- Documentation in `docs/`
- [The main example project](https://gitlab.catalyst.net.nz/cove/example-project)
- [The example project with external image](https://gitlab.catalyst.net.nz/cove/example-project-external-image)
- [The example project with external chart](https://gitlab.catalyst.net.nz/cove/example-project-external-chart)
- [The example project with external chart and image](https://gitlab.catalyst.net.nz/cove/example-project-deploy-only)
- [Cove deployment documentation](https://gitlab.catalyst.net.nz/cloud-native/cove/-/blob/master/docs/deployment.md)
- [Cove nightly tests](https://gitlab.catalyst.net.nz/cloud-native/cove/-/blob/master/.gitlab-ci/project.yml)
- Do you need to bump the `COVE_DEPLOYMENT_MODULES_VERSION` variable?

### New feature or new version

Breaking changes always trigger a new major version. These should always go
into `master` first, and then into a new tag and version branch.

1. Create a merge request against `master`.
1. Once reviewed and merged, create a new branch `vX` from `master`.
1. Update the changelog and create a new tag `vX.Y.Z` from `vX`.

### Patches to an existing version

**Note** don't make any breaking changes to an existing version! They exist to
provide stability for downstream users, so they should only receive bugfixes.

1. Create a merge request against the relevant branch version `vX`. Mention
   if this fix applies to any other version branches and/or `master`.
1. Once merged, create a new tag `vX.Y.Z` incrementing the minor and/or patch
   version as needed.
1. If required:
   1. cherry-pick the fix onto other release branches and/or `master`,
   1. tag new versions for any branches that received the fix, and
   1. if changes were made to the latest version, merge back to `master`.

### Changelog

Add a CHANGELOG.md entry if your contribution deems it necessary!

[semantically versioned releases]: https://semver.org/
