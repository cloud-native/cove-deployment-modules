#!/bin/bash

# This script deploys a Cove app.
# It is intended to be used as part of the CDM CI pipeline.

set -o errexit -o nounset -o pipefail -o noclobber

# shellcheck source-path=SCRIPTDIR
. "${COVE_CDM_ROOT}/scripts/cove-helpers.sh"

require_variables "COVE_NAMESPACE IMAGE_REPOSITORY IMAGE_REGISTRY_SECRET_NAME IMAGE_REGISTRY_USER IMAGE_REGISTRY_PASSWORD COVE_VALUES_FILE_FINAL"

set_up_helm  # sets $chart_path and $chart_version

if ! could_deploy_clobber "${COVE_NAMESPACE}"; then
  exit 1
fi

# provision namespace
create_namespace "${COVE_NAMESPACE}"

if [ -d "$COVE_PRE_DEPLOY_MANIFEST_DIR" ]; then
  echo "Pre-deploying manifests at $COVE_PRE_DEPLOY_MANIFEST_DIR"
  kubectl apply -n "$COVE_NAMESPACE" -f "$COVE_PRE_DEPLOY_MANIFEST_DIR"
fi

# determine the image registry from the image repo
IMAGE_REGISTRY=${IMAGE_REPOSITORY/\/*}
# provision image registry secret
kubectl create secret docker-registry "${IMAGE_REGISTRY_SECRET_NAME}" \
  --docker-server="${IMAGE_REGISTRY}" \
  --docker-username="${IMAGE_REGISTRY_USER}" \
  --docker-password="${IMAGE_REGISTRY_PASSWORD}" \
  --output=yaml --dry-run=client \
| kubectl apply -f -

# deploy!
helm upgrade "${COVE_NAMESPACE}" "${chart_path:?}" --install \
  --values "${COVE_VALUES_FILE_FINAL}" --version "${chart_version:?}" ${COVE_HELM_DEPLOY_FLAGS}

kubectl get pod
