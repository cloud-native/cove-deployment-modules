#!/bin/bash

# This script deletes a Cove app.
# It is intended to be used as part of the CDM CI pipeline.

set -o errexit -o nounset -o pipefail -o noclobber

# shellcheck source-path=SCRIPTDIR
. "${COVE_CDM_ROOT}/scripts/cove-helpers.sh"

require_variables "COVE_NAMESPACE"

delete_namespace "${COVE_NAMESPACE}"
