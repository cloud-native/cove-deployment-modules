#!/bin/bash

# A script used by CI to validate a Cove app, prior to deployment.

# shellcheck source-path=SCRIPTDIR
. "${COVE_CDM_ROOT}/scripts/cove-helpers.sh"

configure_script # errexit et al
require_variables "COVE_NAMESPACE COVE_VALUES_FILE_FINAL"

template_file="helm-template-${CI_JOB_ID}" # holds output of 'helm template' command
temporary_ns="validate-cove-app-${CI_JOB_ID}" # only used if COVE_NAMESPACE doesn't exist

set_up_helm  # sets $chart_path and $chart_version

cleanup() {
  echo "Cleaning up..."
  delete_namespace "${temporary_ns}"
  rm -f "${template_file}"
}

filter_rolebindings() {
  yq -y 'select(.kind != "RoleBinding") | select(. != null)'
}

if ! could_deploy_clobber "${COVE_NAMESPACE}"; then
  exit 1
fi

# use $COVE_NAMESPACE for validation if exists
if managed_namespace_exists "${COVE_NAMESPACE}"; then
  validation_ns="${COVE_NAMESPACE}"
# otherwise create a temporary namespace for validation
elif create_namespace "${temporary_ns}"
then
  validation_ns="${temporary_ns}"
else
  echo "Unable to set up temporary namespace for validation 👎️"
  echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#unable-to-set-up-temporary-namespace-for-validation"
  cleanup
  exit 1
fi

# normalize pre-deploy manifests as YAML and add to template file
if [ -n "$(echo "${COVE_PRE_DEPLOY_MANIFEST_DIR}"/*.{json,y*ml})" ]; then
  echo "Generating pre-deploy manifests..."
  yq --yaml-output --explicit-start --explicit-end --slurp '.[]' "${COVE_PRE_DEPLOY_MANIFEST_DIR}"/*.{json,y*ml} > "${template_file}"
fi

echo "Generating resource manifests (chart version ${chart_version:?})..."
helm template "${COVE_NAMESPACE}" "${chart_path:?}" >> "${template_file}" \
  --no-hooks --version "${chart_version:?}" \
  --namespace "${validation_ns}" --values "${COVE_VALUES_FILE_FINAL}"

if [ "${COVE_HELM_DIFF:-false}" != false ]; then
  echo "Generating diff (chart version ${chart_version:?})..."
  filter_rolebindings < "${template_file}" | kubectl diff -n "${validation_ns}" -f- --server-side --force-conflicts || :
fi

echo "Validating deploy (chart version ${chart_version:?})..."
if filter_rolebindings < "${template_file}" | kubectl apply -n "${validation_ns}" -f- --dry-run=server --server-side --force-conflicts
then
  echo "Ready to roll! ✨"
  cleanup
else
  echo "Deploy validation failed 👎️"
  echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#deploy-validation-failed"
  cleanup
  exit 1
fi
