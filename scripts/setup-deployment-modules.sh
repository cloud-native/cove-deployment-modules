#!/bin/bash

set -o errexit -o nounset -o pipefail -o noclobber

# shellcheck source-path=SCRIPTDIR
. "${COVE_CDM_ROOT}/scripts/cove-helpers.sh"

require_variables "COVE_NAMESPACE COVE_AGENT COVE_AGENT_REPOSITORY COVE_VALUES_FILE_FINAL"

# If COVE_AGENT is set but KUBECONFIG is not, it means the agent isn't
# configured correctly. Point the user in the right direction.
test -v KUBECONFIG || {
  echo "KUBECONFIG is not set 👎"
  echo "This usually means the GitLab agent you've specified can't be used for some reason."
  echo "Does the ${COVE_AGENT} agent exist in the ${COVE_AGENT_REPOSITORY} project?"
  echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#xyz-is-not-set"
  exit 1
}

echo -n "Correcting $KUBECONFIG permissions... "
chmod 600 $KUBECONFIG
echo "OK!"

touch "${COVE_AGENT}-${COVE_NAMESPACE}-values.env"
touch "${COVE_VALUES_FILE_FINAL}"

# shellcheck disable=SC2046
export CI $(xargs < "${COVE_AGENT}-${COVE_NAMESPACE}-values.env")

echo "Available kubeconfig contexts:"
kubectl config get-contexts
echo "Using context ${COVE_AGENT_REPOSITORY}:${COVE_AGENT}"
kubectl config use-context "${COVE_AGENT_REPOSITORY}:${COVE_AGENT}"
kubectl config set-context --current --namespace "${COVE_NAMESPACE}"
