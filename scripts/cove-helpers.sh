#!/bin/bash

# common prelude for shell scripts
configure_script() {
  set -o errexit -o nounset -o pipefail -o noclobber
  shopt -s nullglob
}

# get the rancher cluster id
# we determine this from an annotation of a namespace that's part of a project
# in rancher ;)
get_cluster_id() {
  kubectl get ns cattle-system -o json |
    jq -r '.metadata.annotations."field.cattle.io/projectId" // "unknown:unknown"' |
    cut -d: -f1
}

# create a namespace inside the project RANCHER_PROJECT_ID.
create_namespace() {
  local namespace="${1:?}"

  cat <<EOF | kubectl apply -f -
    apiVersion: v1
    kind: Namespace
    metadata:
      name: ${namespace}
      annotations:
        field.cattle.io/projectId: $(get_cluster_id):${COVE_RANCHER_PROJECT_ID:=__unset__}
        catalystcove.nz/deployed-by: ${GITLAB_USER_EMAIL:?}
        catalystcove.nz/gitlab-agent: ${COVE_AGENT:?}
        catalystcove.nz/managed-by: ${COVE_TEAM:?}
        pod-security.kubernetes.io/enforce: ${COVE_POD_SECURITY_ENFORCE:?}
        pod-security.kubernetes.io/warn: ${COVE_POD_SECURITY_WARN:?}
EOF

  echo 1>&2 "Waiting for namespace..."

  # check gitlab agent access to namespace
  timeout 30s bash -c "until kubectl -n ${namespace} get role cove-gitlab-agent &> /dev/null; do sleep 2; done"
}

could_deploy_clobber() {
  # Check if a namespace is managed by the expected agent and team/project.
  # This is to avoid clobbering matching namespaces in different clusters,
  # the namespace not existing will also pass this check.
  require_variables "COVE_AGENT COVE_TEAM"

  local -r ns_json=$(kubectl get namespace --ignore-not-found "${1:?}" -o json)
  local gitlab_agent
  local managed_by

  gitlab_agent=$(echo "$ns_json" | jq -r '.metadata.annotations."catalystcove.nz/gitlab-agent"')
  managed_by=$(echo "$ns_json" | jq -r '.metadata.annotations."catalystcove.nz/managed-by"')

  if [ -z "$ns_json" ]; then
    return 0
  elif [ "$gitlab_agent" != "$COVE_AGENT" ]; then
    echo "Namespace $1 uses the $gitlab_agent agent, refusing to deploy."
    echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#using-an-existing-namespace-with-cdm"
    return 1
  elif [ "$managed_by" != "$COVE_TEAM" ]; then
    echo "Namespace $1 is managed by $managed_by, refusing to deploy."
    echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#using-an-existing-namespace-with-cdm"
    return 1
  fi
}

managed_namespace_exists() {
  # check if a namespace exists and is managed by the gitlab agent
  kubectl get namespace --ignore-not-found "${1:?}" -o json | jq -e '.metadata.annotations."catalystcove.nz/gitlab-agent"' &> /dev/null
}

delete_namespace() {
  if managed_namespace_exists "${1:?}"; then
    # the reason for checking namespace existence like this, and not just relying
    # on --ignore-not-found, is so we can avoid permission issues listing namespaces
    # speaking of which, we have to include --wait=false due to this issue:
    # - https://github.com/kubernetes/kubectl/issues/1411
    kubectl delete namespace --ignore-not-found "${1:?}" --wait=false
  fi
}

set_up_helm() {
  local helm_args=()

  if [ -n "${COVE_CHART_NAME}" ] && [ -n "${COVE_CHART_VERSION}" ]; then
    # we're fetching the chart from a remote chart repo
    require_variables "COVE_CHART_REPO"
    if [ -n "${COVE_CHART_USER}" ] && [ -n "${COVE_CHART_PASSWORD}" ]; then
      # credentials provided -> pass them to helm
      helm_args=(--username "${COVE_CHART_USER}" --password "${COVE_CHART_PASSWORD}")
    fi

    helm repo add helm-repo "${COVE_CHART_REPO:?}" "${helm_args[@]}"
    helm repo update helm-repo

    chart_path="helm-repo/${COVE_CHART_NAME}"
    chart_version=${COVE_CHART_VERSION}
  else
    # use the in-project chart; determine the version from it
    require_variables "COVE_CHART_DIR"
    # shellcheck disable=SC2034
    chart_path="${COVE_CHART_DIR:?}"
    # shellcheck disable=SC2034
    chart_version=$(yq -r .version "${chart_path}"/Chart.yaml)
    # If there are chart dependencies, download them
    if yq -e '.dependencies[]?' "${chart_path}"/Chart.yaml &> /dev/null; then
      yq -r '.dependencies[].repository | select(startswith("oci://") | not)' "${chart_path}"/Chart.yaml | xargs --verbose -I "{url}" bash -c 'helm repo add "$$" "{url}"'
      helm dependency build "${chart_path}"
    fi
  fi
}

require_variables() {
  local variables="${1:?}"
  local has_unset_variables=false

  for v in $variables; do
    if [ -z "${!v}" ]; then
      echo "$v is not set 👎"
      echo "See https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/latest/#xyz-is-not-set"
      has_unset_variables=true
    fi
  done

  if [ "$has_unset_variables" = true ]; then
    exit 1
  fi
}

append_image_digests() {
# append the image digests to the values env file
  for digest_file in $(find . -type f -name "*.${IMAGE_DIGEST_EXTENSION}" -printf "%f\n"); do
  # strip off the extension and convert the name into an environment variable name
    digest_var_name="$(printf "%s" "${digest_file%".$IMAGE_DIGEST_EXTENSION"}" |  tr -s -c [:alnum:] _ | tr [:lower:] [:upper:])_DIGEST"
    echo "Adding ${digest_var_name} variable to values env file."
    echo "You can use this to reference your images to pull, instead of tags ;)"
    echo "${digest_var_name}=$(cat "${digest_file}")" >> "${COVE_AGENT}-${COVE_NAMESPACE}-values.env"
  done
}
