#!/bin/bash

# This script generate mkdocs for the VERSIONS (branches) specified.
# It uses mike to aid with generating versioned documentation.
# It is currently only used by CI for building the CDM project's docs.

set -o errexit -o nounset -o pipefail -o noclobber

CDM_REPO="https://ci-token:${CDM_ACCESS_TOKEN}@gitlab.catalyst.net.nz/cloud-native/cove-deployment-modules.git"
BUILD_DIR="$(mktemp -d --suffix cdm-docs)"
PUBLISH_BRANCH="cdm-pages"
PUBLISH_DIR="${1:?}"  # use a fullpath here

# from oldest, to newest
VERSIONS=(v7 v8)

git clone "${CDM_REPO}" "${BUILD_DIR}"
cd "${BUILD_DIR}"
# configure git slightly, so mike works
git config --add user.name ci-boss
git config --add user.email ci-boss

for version in "${VERSIONS[@]}"; do
  git checkout "${version}"
  mike deploy -b "${PUBLISH_BRANCH}" "${version}"
done

echo "Setting ${VERSIONS[-1]} as the default/latest documentation"
mike alias -b "${PUBLISH_BRANCH}" "${VERSIONS[-1]}" latest
mike set-default -b "${PUBLISH_BRANCH}" latest

mkdir "${PUBLISH_DIR}"
git archive "${PUBLISH_BRANCH}" | tar --extract --directory "${PUBLISH_DIR}"
