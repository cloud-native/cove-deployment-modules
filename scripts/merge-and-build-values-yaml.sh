#!/bin/bash

set -o errexit -o nounset -o pipefail -o noclobber

# shellcheck source-path=SCRIPTDIR
. "${COVE_CDM_ROOT}/scripts/cove-helpers.sh"

require_variables "COVE_NAMESPACE COVE_AGENT VALUES_FILE"

cd "${CI_PROJECT_DIR}"

touch "${COVE_AGENT}-${COVE_NAMESPACE}-values.env"

# append the image digests to the values env file
append_image_digests

export CI $(xargs < ${COVE_AGENT}-${COVE_NAMESPACE}-values.env)

for FILE in ${VALUES_FILE} ; do
    touch $FILE
    envsubst $VALUES_SUBSTITUTE_VARS < $FILE > $FILE.substituted
    mv $FILE.substituted $FILE
done
cat ${VALUES_FILE} | yq --slurp --yaml-output add > merged-values-file.yaml.orig
