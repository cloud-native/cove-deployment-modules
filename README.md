# Cove Deployment Modules

Extendable CI job templates for building and deploying your apps to Cove quickly
and uniformly.

View the
[documentation here](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules)
or see the [`docs` folder](./docs/index.md).

For contributing [this documentation](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/v8/contributing)
or the [repo version](./docs/contributing.md)