# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [8.8.0] - 2024-02-27

- Multiple files can be specified with the `VALUES_FILE` environment variable
and these will be merged together in order

### Added

- `--wait-for-jobs --timeout=30m` was added to `COVE_HELM_DEPLOY_FLAGS` defaults

## [8.7.0] - 2024-11-27

### Added

- `--wait-for-jobs --timeout=30m` was added to `COVE_HELM_DEPLOY_FLAGS` defaults

## [8.6.0] - 2024-11-21

### Fixed

- `KANIKO_FLAGS` after GitLab upgrade, when using `cove-image-build.yaml` directly

### Added

- Configure Hadolint via an `HADOLINT_OVERRIDES` variable
- Validation for `COVE_PRE_DEPLOY_MANIFEST_DIR` manifests

## [8.5.0] - 2024-08-22

### Added

- Use Cove's in-cluster docker registry cache for kaniko caching - configured via
  the `KANIKO_FLAGS_CACHING` variable

## [8.4.0] - 2024-07-08

### Added

- The `.validate-cove-app` job can now generate diff output to preview planned
  changes by by setting `COVE_HELM_DIFF: 'true'`
- The file extension for image digests created by the `.build-image` job can
  now be customized with the `IMAGE_DIGEST_EXTENSION` variable

### Changed

- CI job templates now use a CDM-specific image tag by default
- Validation for CI variables has been improved
- Validation error messages now link to the relevant documentation
- Spurious "namespace not found" messages have been removed from CI output
- The Kaniko image has been updated from `v1.9.1` to `v1.22.0`

### Removed

- The `$BUILD_NAME-latest` tag from the `.build-image` job

## [8.3.0] - 2024-06-04

### Added

- Helm flags can be changed with the `COVE_HELM_DEPLOY_FLAGS` variable
- The `.validate-cove-app` and `.deploy-cove-app` jobs will now fetch chart
  dependencies when installing from a local Helm chart

### Changed

- Deploy jobs will now wait for Helm operations to finish by default
- Image scan results are now written to build-specific filenames to avoid collisions

## [8.2.2] - 2024-01-30

### Fixed

- Error caused by an incorrect check for existing namespaces

## [8.2.1] - 2024-01-18

### Added

- Support for public Helm repositories

### Changed

- Minor improvements to messages logged to console when validating deploys
  within a temporary namespace

## [8.2.0] - 2023-09-15

### Added

- Protections have been added to try and prevent accidentally clobbering deployments
  by pointing a job at the wrong cluster
- The ability to deploy manifests before the main app deployment with the
  `COVE_PRE_DEPLOY_MANIFEST_DIR` variable

## [8.1.0] - 2023-06-24

### Added

- The ability to configure pod security admission via the
  `COVE_POD_SECURITY_ENFORCE` and `COVE_POD_SECURITY_WARN` variables

## [8.0.0] - 2023-05-10

### Added

- Use GitLab Kubernetes Agents to deploy more easily!
  See the [new docs here](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/v8/).

  If you're migrating from `v7`:
  - In your `values.yaml` template file, the following placeholders have changed:
    - `$CI_IMAGE_REGISTRY` to `$IMAGE_REPOSITORY`
    - `$REGISTRY_SECRET_NAME` to `$IMAGE_REGISTRY_SECRET_NAME`
  - You no longer need to configure a `COVE_CONFIG` CI/CD variable :)
    Instead, you set the required variables straight in your `.gitlab-ci.yaml` file:
      - `chart_name` is now the `COVE_CHART_NAME` variable
      - `chart_version` is now the `COVE_CHART_VERSION` variable
      - `targets` are set via `COVE_AGENT` and `COVE_RANCHER_PROJECT_ID`
  - You no longer need
    [plan](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/v7/#the-plan-job)
    and [state management](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove-deployment-modules/v7/#the-plan-job)
    jobs, so go ahead and remove those.
  - Use the [example project](https://gitlab.catalyst.net.nz/cove/example-project/-/tree/main)
    as further guidance

## [7.4.0] - 2023-03-03

### Added

- `.build-image`: Labels for debugging:
  - `org.opencontainers.image.pipeline` - pipeline ID
  - `org.opencontainers.image.job` - job ID
  - `org.opencontainers.image.revision` - ${CI_COMMIT_SHORT_SHA} value
- Option to delete GitLab Terraform state upon app deletion, by setting
  `COVE_STATE_DELETE: 'true'` in your job that extends `.delete-cove-app`
- `.validate-cove-app` - extend this to validate your Cove app prior to
  deployment, by dry-running your deployment against your cluster
- `.copy-image` allows you to copy an image to another registry (e.g. Harbor)
  by setting `IMAGE_REPOSITORY` and `IMAGE_REGISTRY_{USER,PASSWORD}`

### Changed

- Update Cove tools image to latest version
- Update rancher2 terraform provider to latest version
- Update kaniko to latest version
- Lots of refactoring!

### Fixed

- GitLab resource group locking that was too aggressive

## [7.3.0] - 2022-06-22

### Added

- `.build-image`: Arbitrary flags can be passed to Kaniko via the new `KANIKO_FLAGS` variable.
- `.build-image`: The target build stage can now be specified independently of the image name via the new `BUILD_TARGET` variable.

## [7.2.0] - 2022-06-08

### Added

- Helm chart linting - [docs here](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#the-lint-helm-chart-job)

## [7.1.0] - 2022-05-19

### Added

- Helm chart management. This allows you to deploy from an in-project Helm
chart, rather than relying on an existing Helm repo. See the documentation
[here][build-helm-chart]. Also ensure that you update your deploy token to
have `read_package_registry` access (see [GitLab deploy token docs]).
- Dockerfile linting - [docs here](https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#the-lint-dockerfile-job)

[gitlab deploy token docs]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#create-gitlab-deploy-token
[build-helm-chart]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#the-build-helm-chart-job

## [7.0.0] - 2022-03-14

### Added

- Terraform state administration. See the documentation [here][state-management-jobs].
- Cove admin tooling. See the documentation [here][cove-admin-tooling].

[state-management-jobs]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#state-management-jobs
[cove-admin-tooling]: https://cloud-native.pages.gitlab.wgtn.cat-it.co.nz/cove/deployment/#cove-admin-tooling

### Changed

- The modules now deploy [Rancher v2 apps]. At this stage, in order to
upgrade from `v6`, you'll have to completely delete your app's namespace in
Rancher, then re-deploy.
- `.build-image`: Dropped the requirement to have `project-` prepended to the
build stage names in your Dockerfile.
When upgrading, either remove `project-` from your Dockerfile, or update your
`BUILD_NAME` variable to include `project-`.

[Rancher v2 apps]: https://rancher.com/docs/rancher/v2.5/en/helm-charts/

### Deprecated

- `.verify-cove-app`: If you're making use of this configuration, you should
amend to extending `.cove-admin` instead, for example:

```yaml
verify-staging:
  extends: .cove-admin
  stage: verify
  variables:
    COVE_TARGET: staging
  script:
    - kubectl wait --for condition=available deploy/example-application
```
